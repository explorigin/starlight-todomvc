# [Starlight](https://gitlab.com/explorigin/starlight) • [TodoMVC](http://todomvc.com)

Starlight is a virtual-DOM framework that completely decouples application code from the DOM.

## Resources

- [Website](https://gitlab.com/explorigin/starlight)
- [Documentation](https://gitlab.com/explorigin/starlight/wikis/home)
- [Used by](https://gitlab.com/explorigin/starlight/wikis/usedby)
- [FAQ](https://gitlab.com/explorigin/starlight/wikis/faq)

## Credit

Created by [Timothy Farrell](https://github.com/explorigin)
