var App = App || { views: {} };

(function () {
	'use strict';

    const ENTER_KEY = 13, ESC_KEY = 27;

    App.views.todo = function TodoView(api, props) {
        const editing = api.prop(false, api.redraw);
        const item = props.item;
        const store = api.store;
        let editorValue = item.text;

        function toggleCompleted() {
            store.editComplete(!item.completed, item.id);
            props.onChange();
        }

        function setEditing() {
            editing(true);
        }

        function remove() {
            store.remove(item.id);
            props.onChange();
        }

        function saveEdit() {
            store.editText(editorValue, item.id);
            editing(false);
        }

        function editorKeyup(evt) {
            editorValue = evt.target.value;

            switch (evt.keyCode) {
            case ENTER_KEY:
                saveEdit();
                break;
            case ESC_KEY:
                editing(false);
            }
        }

        return function() {
            return ['li',
                {
                    class: {
                        completed: item.completed,
                        editing: editing()
                    },
                },
                (
                    !editing()
                    ? ['.view',
                            ['input.toggle[type=checkbox]', {
                                checked: item.completed,
                                onclick: toggleCompleted
                            }],
                            ['label', { ondblclick: setEditing }, item.text],
                            ['button.destroy', { onclick: remove }]
                    ]
                    : ['input.edit', {
                        value: item.text,
                        onkeyup: editorKeyup,
                        onblur: saveEdit,
                        focus: editing(),
                        select: editing(),
                    }]
                )
            ];
        };
    };
})();
