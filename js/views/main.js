var App = App || { views: {} };

(function () {
	'use strict';

    const ENTER_KEY = 13;

    App.views.main = function MainView(api, filter) {
        var href = api.href;
        var newTodoValue = api.prop('', api.redraw);
        var toggleAllChecked = api.prop();
        var store = api.store;

        function todoInputKeyStroke(evt) {
            newTodoValue(evt.target.value.trim());

            if (evt.keyCode === ENTER_KEY) {
                store.add(newTodoValue());
                newTodoValue('');
                update();
            }
        }

        function toggleAll(e) {
            const completed = e.target.checked;
            store.getItems(filter)
                .map(i => i.id)
                .forEach(id => store.editComplete(completed, id));
            update();
        }

        function update() {
            toggleAllChecked(store.getItems().every(i => i.completed));
            api.redraw();
        }

        function clearCompleted() {
            store.removeCompleted();
            update();
        }

        update();

        return function() {
            const count = store.count();
            const hasItems = count > 0;
            const hasCompleted = store.getItems('completed').length;

            return [
                ['header.header',
                    ['h1', 'todos'],
                    ['input.new-todo[autofocus]', {
                        placeholder: 'What needs to be done?',
                        onkeyup: todoInputKeyStroke,
                        value: newTodoValue
                    }]
                ],
                ['section.main', {style: hasItems ? '' : 'display: none' },
                    ['input.toggle-all[type=checkbox]',
                        {
                            onclick: toggleAll,
                            checked: toggleAllChecked()
                        }
                    ],
                    ['label', {for:'toggle-all'}, 'Mark all as complete'],
                    ['ul.todo-list',
                        store.getItems(filter).map(function (i) {
                            return [App.views.todo, {item: i, onChange: update}];
                        })
                    ]
                ],
                ['footer.footer', {style: hasItems ? '' : 'display: none'},
                    ['span.todo-count',
                        ['strong', count],
                        ` item${count === 1 ? '' : 's'} left`
                    ],
                    ['ul.filters',
                        ['li', ['a', {href: href('home'), class: {selected: filter === 'all'}}, 'All']],
                        ['li', ['a', {href: href('filter', {filter: 'active'}), class: {selected: filter === 'active'}}, 'Active']],
                        ['li', ['a', {href: href('filter', {filter: 'completed'}), class: {selected: filter === 'completed'}}, 'Completed']],
                    ],
                    ['button.clear-completed',
                        {
                            onclick: clearCompleted,
                            style: hasCompleted ? '' : 'display: none'
                        },
                        'Clear Completed'
                    ]
                ]
            ];
        };
    }
})();
