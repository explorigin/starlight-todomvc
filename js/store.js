var App = App || { views: {} };

(function() {
	'use strict';

    const STORAGE_ID = 'todos-starlight';
    let todos = [];

    function getFilter(filterName) {
        const filters = {
            all: function(i) { return true; },
            active: function(i) { return !i.completed; },
            completed: function(i) { return i.completed; }
        };
        return filters[filterName] || filters.all;
    }

    function Todo(text) {
        this.id = '' + Date.now() + String(Math.random()).substr(2, 10);
        this.text = text;
        this.completed = false;
    };

    function save() {
        localStorage[STORAGE_ID] = JSON.stringify(todos);
    }

    function restore() {
        try {
            todos = JSON.parse(localStorage[STORAGE_ID]);
        } catch(e) {
            todos = [];
            save();
        }
    }

    function mutate(fn, id) {
        var editCount = 0;
        todos = todos.map(function(item) {
            if (item.id === id) {
                fn(item);
                editCount += 1;
            }
            return item;
        });

        return editCount;
    }

    function filter(fn) {
        var editCount = todos.length;
        todos = todos.filter(fn);

        return todos.length - editCount;
    }

    function getItems(filter) {
        return todos.filter(getFilter(filter));
    }

    function editComplete(completed, id) {
        if (mutate(function(i) { i.completed = completed; }, id)) {
            save();
        }
    }

    function editText(text, id) {
        if (id === undefined) {
            todos.push(new Todo(text));
        } else {
            mutate(function(i) { i.text = text; }, id);
        }
        save();
    }

    function remove(id) {
        filter(function(item) { return item.id !== id; });
        save();
    }

    function removeCompleted() {
        filter(function(item) { return !item.completed; });
        save();
    }

    function count() {
        return todos.length;
    }

    function remaining() {
        return getItems('active').length;
    }

    restore();

    App.store = {
        add: editText,
        remove: remove,
        editText: editText,
        save: save,
        getItems: getItems,
        editComplete: editComplete,
        removeCompleted: removeCompleted,
        count: count,
        remaining: remaining,
    };
})();
