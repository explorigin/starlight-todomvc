(function (window) {
	'use strict';

    var router = Starlight.Router(
        '#',
        {
            home: {
                path: '/',
                onenter: function(r, route) {
                    app.update('all');
                }
            },
            filter: {
                path: '/:filter',
                vars: { filter: /(all|completed|active)/ },
                onenter: function (r, route) {
                    app.update(route.vars.filter);
                }
            }
        }
    );

    var app = Starlight.mount(
        App.views.main,
        document.getElementsByClassName('todoapp')[0],
        Object.assign({
            store: App.store,
            href: router.href,
        }, Starlight.addons)
    );

    router.listen('home');
})(window);
